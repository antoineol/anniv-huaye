import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/debounceTime';

@Injectable()
export class WindowResizeService {

  public readonly resize = Observable.fromEvent(window, 'resize');
  public readonly resizeDebounced = this.resize.debounceTime(200);

  constructor() {}

}
