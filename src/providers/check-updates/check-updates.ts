import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import 'rxjs/add/operator/mergeMap';

declare global {
  interface Window {
    swObservable: any;
  }
}

@Injectable()
export class CheckUpdatesProvider {

  canRefresh: boolean;

  constructor(private toastCtrl: ToastController) {
  }

  checkUpdates(): void {
    if (typeof window !== 'undefined' && window.swObservable) {
      window.swObservable.subscribe((hasUpdate: boolean) => {
        this.canRefresh = hasUpdate;

        const config = hasUpdate ?
          {
            message:         'Mise à jour disponible, rechargez la page pour mettre à jour (bouton en haut).',
            showCloseButton: true,
            closeButtonText: 'Fermer',
          } :
          {
            message:  'L\'application est maintenant accessible hors-ligne.',
            duration: 3000,
          };

        let toast = this.toastCtrl.create(config);

        toast.present();
      });
      // window.swObservable.next(true); // for debugging purpose
    }
  }

}
