import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { CheckUpdatesProvider } from '../check-updates/check-updates';

/*
  Generated class for the ErrorService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ErrorService {

  constructor(private toastCtrl: ToastController, private checkUpdate: CheckUpdatesProvider) {
  }


  handle(message?: string | true) {
    return (error: any) => {
      if (message && message !== true) {
        console.error(message);
      }
      console.error(error);

      const tipMessage = message === true
        ? 'Navré, une erreur bizarre gêne l\'application. Vous pouvez essayer de recharger la page. ' +
        'Prévenez le développeur si l\'erreur persiste.'
        : error && error.message || error;
      let toast        = this.toastCtrl.create({
        message:         tipMessage.substr(0, 500),
        showCloseButton: true,
        closeButtonText: 'Fermer',
      });
      toast.present();

      this.checkUpdate.canRefresh = true;
    };
  }

}
