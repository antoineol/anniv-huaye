import { Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { WindowResizeService } from '../../providers/window-resize.service';


// From https://www.w3schools.com/graphics/game_intro.asp - section try it yourself

const smallWinScore = 1500;

interface InGameComponentOptions {
  width?: number;
  height?: number;
  fontSize?: string;
  fontFamily?: string;
  color?: string;
  source?: string;
  x: number;
  y: number;
  type?: 'text' | undefined;
}

interface SvgComponentOptions {
  width: number;
  height: number;
  source: string;
  x: number;
  y: number;
  type: 'img';
}

class InGameComponent {
  gravity: number              = 0;
  private speedX: number       = 0;
  private speedY: number       = 0;
  private gravitySpeed: number = 0;
  public text                  = '';
  private img: HTMLImageElement;
  private imgLoaded: boolean;

  constructor(private canvas: HTMLCanvasElement,
              private context: CanvasRenderingContext2D,
              private width: number | string,
              private height: number | string,
              private color: string,
              public x: number,
              private y: number,
              private options?: InGameComponentOptions | SvgComponentOptions) {
    if (options) {
      this.x = this.options.x;
      this.y = this.options.y;
    }
    if (this.options && this.options.type === 'img') {
      const img = this.img = new Image();
      img.onload = () => this.imgLoaded = true;
      img.src = this.options.source;
    }
  }

  update() {
    const ctx = this.context;
    if (this.options && this.options.type === 'text') {
      ctx.font      = this.options.fontSize + ' ' + this.options.fontFamily;
      ctx.fillStyle = this.color;
      ctx.fillText(this.text, this.x, this.y);
    } else if (this.options && this.options.type === 'img') {
      if (this.imgLoaded) {
        ctx.drawImage(this.img, this.x, this.y, this.options.width, this.options.height);
      }
    } else {
      ctx.fillStyle = this.color;
      ctx.fillRect(this.x, this.y, this.width as number, this.height as number);
    }
  }

  newPos(numberOfFrames: number) {
    for (let i = 1; i <= numberOfFrames; i++) {
      this.gravitySpeed += this.gravity;
      this.x += this.speedX;
      this.y += this.speedY + this.gravitySpeed;
    }
    this.hitTop();
    this.hitBottom();
  }

  hitTop() {
    const rockTop = 0;
    if (this.y <= rockTop) {
      this.y            = rockTop;
      this.gravitySpeed = 0;
    }
  }

  hitBottom() {
    const rockBottom = this.canvas.height - (this.height as number);
    if (this.y > rockBottom) {
      this.y            = rockBottom;
      this.gravitySpeed = 0;
    }
  }

  crashWith(otherobj) {
    const myleft      = this.x;
    const myright     = this.x + (this.width as number);
    const mytop       = this.y;
    const mybottom    = this.y + (this.height as number);
    const otherleft   = otherobj.x;
    const otherright  = otherobj.x + (otherobj.width);
    const othertop    = otherobj.y;
    const otherbottom = otherobj.y + (otherobj.height);
    let crash         = true;
    if ((mybottom < othertop) || (mytop > otherbottom) || (myright < otherleft) || (myleft > otherright)) {
      crash = false;
    }
    return crash;
  }

  gameTouchStart() {
    this.accelerate(-0.2);
  }

  gameTouchEnd() {
    this.accelerate(0.05);
  }

  accelerate(n) {
    this.gravity = n;
  }
}

class GameArea {
  private context: CanvasRenderingContext2D;
  private gamePiece: InGameComponent;
  private myScore: InGameComponent;
  private frameNo                      = 0;
  private wallCounter                  = 150;
  private obstacles: InGameComponent[] = [];
  private endResolver: (score: number) => void;
  private smallWinResolver: () => void;
  private paused: boolean;
  private prevTime: number;

  constructor(private canvas: HTMLCanvasElement) {
    this.context           = this.canvas.getContext('2d');
    this.gamePiece         = new InGameComponent(this.canvas, this.context, 30, 30, 'red', 10, 120, {
      width:  30,
      height: 30,
      x:      10,
      y:      120,
      type:   'img',
      source: 'assets/icon/bulldog.svg',
    });
    this.gamePiece.gravity = 0.05;
    this.myScore           = new InGameComponent(this.canvas, this.context, '20px', 'Consolas', 'black', 10, 30, {
      fontSize:   '20px',
      fontFamily: 'Consolas',
      color:      'black',
      x:          10,
      y:          30,
      type:       'text',
    });
  }

  smallWin(): Promise<void> {
    return new Promise<void>(resolve => this.smallWinResolver = resolve);
  }

  start(): Promise<number> {
    const p = new Promise(resolve => this.endResolver = resolve);
    this.runGame();
    return p;
  }

  gameTouchStart() {
    this.gamePiece.gameTouchStart();
  }

  gameTouchEnd() {
    this.gamePiece.gameTouchEnd();
  }

  pause() {
    this.paused = true;
    this.stopGame();
  }

  get isPaused() {
    return this.paused;
  }

  continue() {
    if (!this.paused) {
      // throw new Error('Cannot continue if not paused.');
      return;
    }
    this.paused = false;
    this.runGame();
  }

  private updatePainting(time: number) {
    // guards to stop the game
    if (this.paused) {
      return;
    }
    for (const obstacle of this.obstacles) {
      if (this.gamePiece.crashWith(obstacle)) {
        return this.endGame();
      }
    }
    if (this.smallWinResolver && this.frameNo > smallWinScore) {
      this.smallWinResolver();
      this.smallWinResolver = null;
    }

    // Add a wall if applicable
    if (this.wallPop(150)) {
      const x         = this.canvas.width;
      const minHeight = 20;
      const maxHeight = 200;
      const height    = Math.floor(Math.random() * (maxHeight - minHeight + 1) + minHeight);
      const minGap    = 50;
      const maxGap    = 200;
      const gap       = Math.floor(Math.random() * (maxGap - minGap + 1) + minGap);
      this.obstacles.push(new InGameComponent(this.canvas, this.context, 10, height, 'green', x, 0));
      this.obstacles.push(new InGameComponent(this.canvas, this.context, 10, x - height - gap, 'green', x, height + gap));
    }
    // Update score label
    this.myScore.text = 'SCORE: ' + this.frameNo;

    // Update coordinates
    if (this.prevTime) {
      // TODO update
      const delta = Math.round((time - this.prevTime) / 20);

      this.frameNo += delta; // TODO rename to score
      this.wallCounter += delta;

      for (const obstacle of this.obstacles) {
        obstacle.x -= delta;
      }
      this.gamePiece.newPos(delta);
    }
    this.prevTime = time;

    // Draw
    this.clear();
    for (let i = 0; i < this.obstacles.length; i++) {
      this.obstacles[i].update();
    }
    this.myScore.update();
    this.gamePiece.update();

    // plan next painting
    requestAnimationFrame((time) => this.updatePainting(time));
  }

  private clear() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  private wallPop(every: number): boolean {
    if (this.wallCounter >= every) {
      this.wallCounter -= every;
      return true;
    }
    return false;
  }

  private runGame() {
    // this.interval = setInterval(() => this.updateGameArea(), 20);
    requestAnimationFrame((time) => this.updatePainting(time));
  }

  private stopGame() {
    // clearInterval(this.interval);
    // this.interval = null;
  }

  private endGame() {
    this.stopGame();
    // this.end.next(this.frameNo);
    this.endResolver(this.frameNo);
  }
}


/**
 * Generated class for the GameComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector:    'game',
  templateUrl: 'game.html',
})
export class GameComponent implements OnInit, OnDestroy {

  @ViewChild('canvas') canvasRef: ElementRef;

  @Output() smallWin = new EventEmitter<void>();

  smallWinScore = smallWinScore;
  gameStarted: boolean;
  gameScore: number;
  didSmallWin: boolean;

  private canvas: HTMLCanvasElement;
  private resizeSub: Subscription;

  gameArea: GameArea;

  get gameEnded() {
    return this.gameScore && !this.gameStarted && !this.gameArea.isPaused;
  }


  constructor(private resizeService: WindowResizeService) {
  }

  ngOnInit(): void {
    this.canvas        = this.canvasRef.nativeElement;
    this.canvas.width  = this.canvas.offsetWidth;
    this.canvas.height = this.canvas.offsetHeight;
    this.resizeSub     = this.resizeService.resize.subscribe(() => {
      this.canvas.width  = this.canvas.offsetWidth;
      this.canvas.height = this.canvas.offsetHeight;
    });
    // this.startGame();
  }

  ngOnDestroy() {
    this.resizeSub.unsubscribe();
  }

  startGame() {
    this.gameStarted = true;
    this.gameScore   = 0;
    this.didSmallWin = false;
    this.gameArea    = new GameArea(this.canvas);
    this.gameArea.smallWin().then(() => {
      this.didSmallWin = true;
      this.smallWin.emit();
    });
    this.gameArea.start().then(score => {
      this.gameStarted = false;
      this.gameScore   = score;
    });
  }

  replay() {
    this.startGame();
  }

  doTouchstart(e: TouchEvent) {
    if (this.gameArea) {
      e.preventDefault();
      this.gameArea.gameTouchStart();
    }
  }

  doTouchend(e: TouchEvent) {
    if (this.gameArea) {
      e.preventDefault();
      this.gameArea.gameTouchEnd();
    }
  }

  doMousedown(e: MouseEvent) {
    if (this.gameArea) {
      e.preventDefault();
      this.gameArea.gameTouchStart();
    }
  }

  doMouseup(e: MouseEvent) {
    if (this.gameArea) {
      e.preventDefault();
      this.gameArea.gameTouchEnd();
    }
  }

  pause(e?: MouseEvent) {
    if (e) {
      e.preventDefault();
    }
    if (this.gameArea) {
      this.gameArea.pause();
      this.gameStarted = false;
    }
  }

  continue(e?: MouseEvent) {
    if (e) {
      e.preventDefault();
    }
    if (this.gameArea) {
      this.gameArea.continue();
      this.gameStarted = true;
    }
  }

}
