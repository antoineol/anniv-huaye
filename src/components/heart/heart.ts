import { Component } from '@angular/core';

// From https://codepen.io/suez/pen/ZGWbmE - http://www.hongkiat.com/blog/svg-animations/

/**
 * Generated class for the HeartComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'heart',
  templateUrl: 'heart.html'
})
export class HeartComponent {
}
