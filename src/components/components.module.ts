import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { CakeComponent } from './cake/cake';
import { GameComponent } from './game/game';
import { HeartComponent } from './heart/heart';

@NgModule({
  declarations: [
    HeartComponent,
    CakeComponent,
    GameComponent,
  ],
  imports:      [IonicModule],
  exports:      [
    HeartComponent,
    CakeComponent,
    GameComponent,
  ],
})
export class ComponentsModule {
}
