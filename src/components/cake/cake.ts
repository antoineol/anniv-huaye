import { Component } from '@angular/core';

// From https://codepen.io/fixcl/pen/bsIhn - http://www.hongkiat.com/blog/svg-animations/

/**
 * Generated class for the CakeComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'cake',
  templateUrl: 'cake.html'
})
export class CakeComponent {
}
