import { Directive, EventEmitter, HostBinding, HostListener, Input, OnInit, Output } from '@angular/core';


export const autoPlay = typeof location !== 'undefined' && location.hostname !== 'localhost';
// export const autoPlay = true;

/**
 * Generated class for the MusicPlayerDirective directive.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/DirectiveMetadata-class.html
 * for more info on Angular Directives.
 */
@Directive({
  selector: '[music-player]' // Attribute selector
})
export class MusicPlayerDirective implements OnInit {

  @HostBinding('attr.color') buttonColor        = 'light';
  @HostBinding('attr.icon-only') buttonIconOnly = true;

  @HostBinding('class.paused')
  get buttonPaused() {
    return !this.swing;
  }

  // TODO output: notify that the music finding should become true
  // TODO: add communication to tap anywhere in the page to trigger the first play on chrome mobile

  @Output() manualPlayback = new EventEmitter<void>();
  @Output() find           = new EventEmitter<void>();

  @Input('music-player') audio: HTMLAudioElement;
  @Input() autoPlay: boolean;

  swing: boolean;

  private hasPlayedOnce: boolean;

  get iconName() {
    return /*this.audio.paused*/this.swing ? 'musical-notes' : 'musical-note';
  }

  constructor() {
  }

  ngOnInit() {
    this.audio.onplay  = () => this.startedPlaying();
    this.audio.onpause = () => this.stoppedPlaying();
    this.initAudio();
  }

  initAudio() {
    if (autoPlay && this.autoPlay) {
      this.play();
    }
  }

  reset() {
    this.hasPlayedOnce = false;
    this.swing         = false;
    this.initAudio();
  }

  @HostListener('click', ['$event'])
  toggleMusic($event) {
    if (this.audio.paused) {
      this.play();
    } else {
      this.pause();
    }
  }

  private startedPlaying() {
    this.swing         = true;
    this.hasPlayedOnce = true;
    this.find.emit();
  }

  private stoppedPlaying() {
    this.swing = false;
  }

  play() {
    try {
      const playPromise = this.audio.play();

      // if (!this.hasPlayedOnce) {
      // In browsers that don’t yet support this functionality,
      // playPromise won’t be defined.
      if (playPromise !== undefined) {
        (playPromise as any as Promise<void>)
          .catch((error) => this.manualPlayback.emit());
      }

    } catch (err) {
      console.error('Failed to play (try/catch):', err);
    }
  }

  pause() {
    this.audio.pause();
  }

}
