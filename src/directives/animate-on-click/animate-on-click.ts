import { Directive, HostBinding, HostListener, Input } from '@angular/core';

/**
 * Generated class for the AnimateOnClickDirective directive.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/DirectiveMetadata-class.html
 * for more info on Angular Directives.
 */
@Directive({
  selector: '[animateOnClick]' // Attribute selector
})
export class AnimateOnClickDirective {

  @Input('animateOnClick') animationName: string;

  @HostBinding('style.animationName')
  @HostBinding('style.webkitAnimationName')
  animationNameStyle: string;

  constructor() {
  }

  @HostListener('animationend')
  @HostListener('webkitAnimationEnd')
  animationEnd() {
    this.animationNameStyle = null;
  }

  @HostListener('click')
  click() {
    this.animationNameStyle = this.animationName;
  }

}
