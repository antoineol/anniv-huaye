import { NgModule } from '@angular/core';
import { MusicPlayerDirective } from './music-player/music-player';
import { AnimateOnClickDirective } from './animate-on-click/animate-on-click';
@NgModule({
	declarations: [MusicPlayerDirective,
    AnimateOnClickDirective],
	imports: [],
	exports: [MusicPlayerDirective,
    AnimateOnClickDirective]
})
export class DirectivesModule {}
