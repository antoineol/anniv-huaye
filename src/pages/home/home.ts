import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AlertController, Content, IonicPage, NavController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { GameComponent } from '../../components/game/game';
import { autoPlay, MusicPlayerDirective } from '../../directives/music-player/music-player';
import { CheckUpdatesProvider } from '../../providers/check-updates/check-updates';
import { ErrorService } from '../../providers/error-service/error-service';
import { Counts } from './findings.model';
import { Findings, FindingsService } from './findings.service';


@IonicPage()
@Component({
  selector:    'page-home',
  templateUrl: 'home.html',
})
export class HomePage implements OnInit, OnDestroy {

  // Snow animation:
  // https://codepen.io/jpschwinghamer/pen/QwwbgO

  @ViewChild(Content) ionContent: Content;
  @ViewChild(MusicPlayerDirective) musicPlayer: MusicPlayerDirective;
  @ViewChild(GameComponent) game: GameComponent;

  autoPlay = autoPlay;

  needManualPlayback: boolean;

  findings: Findings;
  counts: Counts;

  keyTapped: boolean;
  private sub: Subscription;

  constructor(private navCtrl: NavController, public checkUpdatesProvider: CheckUpdatesProvider,
              private findingsService: FindingsService, private error: ErrorService,
              private alertCtrl: AlertController) {
  }

  ngOnInit(): void {
    this.sub = this.findingsService.getFindings().subscribe(
      findings => {
        this.findings = findings;
        this.counts   = this.findingsService.counts(findings);
        if (this.counts.justCompleted) {
          this.pauseGame();
          this.scrollTop().then(() => {
            this.navCtrl.push('FinalPage');
          });
        }
      },
      this.error.handle(),
    );
  }

  ionViewWillLeave() {
    this.musicPlayer.pause();
    this.pauseGame();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  refreshPage() {
    if (typeof location !== 'undefined' && location.reload) {
      location.reload();
    }
  }

  pauseGame() {
    if (this.game) {
      this.game.pause();
    }
  }

  tryMusic() {
    this.needManualPlayback = false;
    this.musicPlayer.play();
  }

  // Findings

  private update() {
    this.findingsService.updateFindings(this.findings);
  }

  resetFindings() {
    let confirm = this.alertCtrl.create({
      title:   'Êtes-vous sûr ?',
      buttons: [{text: 'Finalement...'},
        {
          text:    'Oui, sûr !',
          handler: () => {
            this.findingsService.resetFindings();
            // if (this.autoPlay) {
            //   this.musicPlayer.play();
            // }
            this.musicPlayer.reset();
            this.keyTapped = false;
            this.scrollTop();
          },
        },
      ],
    });
    confirm.present();


  }

  findMusic() {
    this.findings.music = true;
    this.update();
  }

  shakeTitle() {
    this.findings.headerTitle = true;
    this.update();
  }

  bounceSubtitle() {
    this.findings.contentTitle = true;
    this.update();
  }

  shadeConfettis() {
    this.findings.confettis = true;
    this.update();
  }

  shadePresentBox() {
    this.findings.presentBox = true;
    this.update();
  }

  reduceCake() {
    this.findings.flyingCake = true;
    this.update();
  }

  winGame() {
    this.findings.gravityGame = true;
    this.update();
  }

  tapKey() {
    this.keyTapped = true;
  }

  private scrollTop(): Promise<void> {
    return this.ionContent.scrollToTop(500);
  }

}
