
export interface Counts {
  current: number;
  remaining: number;
  total: number;
  justCompleted: boolean;
}
