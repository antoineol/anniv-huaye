import { Injectable } from '@angular/core';
import 'rxjs/add/observable/throw';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Counts } from './findings.model';

const findingsKey     = 'findings';
const defaultFindings = {
  headerTitle:  false,
  music:        false,
  contentTitle: false,
  confettis:    false,
  presentBox:   false,
  flyingCake:   false,
  gravityGame:  false,
  leftMenu:     false,
  rightMenu:    false,
};

export type Findings = typeof defaultFindings;

@Injectable()
export class FindingsService {
  private findings = new BehaviorSubject<Findings>(this.getCachedFindings());
  private previousCount: Counts;

  constructor() {
    if (typeof window !== 'undefined') {
      window.addEventListener('storage', (e: StorageEvent) => {
        if (e.key === findingsKey) {
          this.updateFindings(this.findingsFromStr(e.newValue));
        }
      }, false);
    }
  }

  getFindings(): Observable<Findings> {
    return this.findings.map(findings => ({...findings}) as Findings);
  }

  updateFindings(findings: Findings) {
    this.cacheFindings(findings);
    this.findings.next(findings);
  }

  /**
   * Returns the number of findings found, i.e. which flag is true and the total number of findings
   * @param {Findings} findings
   * @returns {number}
   */
  counts(findings: Findings): Counts {
    const values       = Object.keys(findings).map(k => findings[k]);
    const current      = values.reduce((acc, val) => val ? acc + 1 : acc, 0);
    const total        = values.length;
    this.previousCount = {
      current,
      remaining:     total - current,
      total:         total,
      justCompleted: this.previousCount && this.previousCount.current < this.previousCount.total && current === total,
    };
    return this.previousCount;
  }

  resetFindings() {
    this.updateFindings(defaultFindings);
  }

  private getCachedFindings(): Findings {
    return this.findingsFromStr(localStorage.getItem(findingsKey));
  }

  private cacheFindings(findings: Findings) {
    localStorage.setItem(findingsKey, JSON.stringify(findings));
  }

  private findingsFromStr(findingsStr: string): Findings {
    let findings: (Findings | {});
    try {
      findings = JSON.parse(findingsStr || '{}');
    } catch (e) {
      findings = {};
    }
    if (Object.keys(findings).length < Object.keys(defaultFindings).length) {
      findings = Object.assign({}, defaultFindings, findings);
      this.cacheFindings(findings as Findings);
    }
    return findings as Findings;
  }
}
