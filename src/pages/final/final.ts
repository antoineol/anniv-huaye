import { Component, ViewChild } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { autoPlay, MusicPlayerDirective } from '../../directives/music-player/music-player';

// Hack to include the JSON in the app bundle
// import * as album from './album.json';
declare const require: any;
const album: string[] = require('./album.json').album;

/**
 * Generated class for the FinalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage({
  defaultHistory: ['HomePage'],
})
@Component({
  selector:    'page-final',
  templateUrl: 'final.html',
})
export class FinalPage {

  @ViewChild(MusicPlayerDirective) musicPlayer: MusicPlayerDirective;

  needManualPlayback: boolean;
  autoPlay = autoPlay;
  album    = album;

  constructor(/*private navCtrl: NavController, private navParams: NavParams*/) {
  }

  ionViewWillLeave() {
    this.musicPlayer.pause();
  }

  tryMusic() {
    this.needManualPlayback = false;
    this.musicPlayer.play();
  }

}
