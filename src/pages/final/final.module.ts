import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from '../../components/components.module';
import { DirectivesModule } from '../../directives/directives.module';
import { FinalPage } from './final';

@NgModule({
  declarations: [
    FinalPage,
  ],
  imports:      [
    IonicPageModule.forChild(FinalPage),
    ComponentsModule,
    DirectivesModule,
  ],
})
export class FinalPageModule {
}
