import { Injectable } from '@angular/core';
import { IonicErrorHandler } from 'ionic-angular';
import { ErrorService } from '../providers/error-service/error-service';

@Injectable()
export class AppErrorHandler extends IonicErrorHandler {
  constructor(private error: ErrorService) {
    super();
  }

  handleError(err: any) {
    this.error.handle()(err);
  }
}
