// import 'web-animations-js/web-animations.min'; // If has animation issue somewhere
import 'web-animations-js';  // Run `npm install --save web-animations-js`.

/**
 * Date, currency, decimal and percent pipes.
 * Needed for: All but Chrome, Firefox, Edge, IE11 and Safari 10
 */
import 'intl';  // Run `npm install --save intl`.
/**
 * Need to import at least one locale-data with intl.
 */
import 'intl/locale-data/jsonp/en';
