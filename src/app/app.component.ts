import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Findings, FindingsService } from '../pages/home/findings.service';
import { CheckUpdatesProvider } from '../providers/check-updates/check-updates';
import { ErrorService } from '../providers/error-service/error-service';
import { Nav } from 'ionic-angular';

@Component({
  templateUrl: 'app.html',
})
export class MyApp implements OnInit, OnDestroy {
  rootPage: any = 'HomePage';
  findings: Findings;
  private sub: Subscription;
  @ViewChild(Nav) nav: Nav;

  constructor(private checkUpdatesProvider: CheckUpdatesProvider, private findingsService: FindingsService,
              private error: ErrorService) {
    try {
      this.checkUpdatesProvider.checkUpdates();
    } catch (e) {
      this.error.handle()(e);
    }
  }

  ngOnInit() {
    try {
      this.sub = this.findingsService.getFindings().subscribe(
        findings => this.findings = findings,
        this.error.handle(),
      );
      this.nav.swipeBackEnabled = false;
    } catch (e) {
      this.error.handle()(e);
    }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  validateLeftMenu() {
    try {
      this.findings.leftMenu = true;
      this.update();
    } catch (e) {
      this.error.handle()(e);
    }
  }

  validateRightMenu() {
    try {
      this.findings.rightMenu = true;
      this.update();
    } catch (e) {
      this.error.handle()(e);
    }
  }

  private update() {
    this.findingsService.updateFindings(this.findings);
  }
}

