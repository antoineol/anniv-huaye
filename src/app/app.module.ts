import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicApp, IonicModule } from 'ionic-angular';
import { FindingsService } from '../pages/home/findings.service';
import { HomePageModule } from '../pages/home/home.module';
import { CheckUpdatesProvider } from '../providers/check-updates/check-updates';
import { ErrorService } from '../providers/error-service/error-service';
import { WindowResizeService } from '../providers/window-resize.service';
import { AppErrorHandler } from './app-error-handler';
import { MyApp } from './app.component';

@NgModule({
  declarations:    [
    MyApp,
  ],
  imports:         [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      swipeBackEnabled: false,
      preloadModules:   true,
    }),
    BrowserAnimationsModule,
    HomePageModule,
  ],
  bootstrap:       [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers:       [
    {provide: ErrorHandler, useClass: AppErrorHandler},
    CheckUpdatesProvider,
    FindingsService,
    ErrorService,
    WindowResizeService,
  ],
})
export class AppModule {
}
