module.exports = {
  staticFileGlobs: [
    'www/**/*.{js,html,css,mp3,json}',
    // 'www/*.{png,jpg,gif}', // to include home page image - uncomment after we have tested that runtimeCaching works as expected
  ],
  maximumFileSizeToCacheInBytes: 10485760,
  root: 'www',
  stripPrefix: 'www',
  navigateFallback: '/index.html',
  runtimeCaching: [{
    urlPattern: /\/.*?\.(ttf|woff|woff2|eot|svg|png|jpg|gif|ico|json|mp3)/,
    handler: 'cacheFirst'
  }],
};
